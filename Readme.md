# IH Business Pro

**Contributors:** (inkhive)  
**Requires at least:** WordPress 4.4  
**Tested up to:** WordPress 4.9.8
**Version:** 2.0.8
**License:** GPL v3  
**License URI:** http://www.gnu.org/licenses/gpl-3.0.html  
**Tags:** custom-background, two-columns, one-column, custom-colors, flexible-header, theme-options, right-sidebar, grid-layout, custom-menu, threaded-comments, translation-ready, featured-images, blog, full-width-template, e-commerce, footer-widgets, entertainment, photography, featured-image-header

Creative and Beautiful WordPress Theme with a Large Header

## Description

IH Business pro is a very powerful and feature rich WordPress theme designed for Businesses and Corporates. With this theme you get access to tons of Social Icons, Layouts, Colors, Featured Sections and an awesome front page. 
Demo Here: http://demo.inkhive.com/ih-business-full-version/


## Copyright


This theme is 100% GPL. And any external resources used and bundled with the theme are also Fully Compatible with the GPL.

1. Font Awesome, Copyright Dave Gandy, 2015-16
	- Code under MIT License
	- Font under SIL OFL 1.1 
	- http://fortawesome.github.io/Font-Awesome/
	
2. Bootstrap, Copyright Twitter, 2013-16
	- MIT License
	- http://getbootstrap.com
	
3. Hover.css, Copyright Ian Lunn, 2016
	- MIT License
	- http://ianlunn.github.io/Hover/
	
4. Nivo Slider, Copyright (c) 2010-2012 Dev7studios
	- MIT LIcense
	- https://github.com/gilbitron/Nivo-Slider	
	
5. _S/Underscores Framework, Copyright (c) Automattic - 2016 
	- GPL v2
	- http://underscores.me
	
6. Modernizer, Copyright (c) Modernizr.com, 2016		
	- MIT and BSD
	- http://modernizr.com
	
7. Responsive Menu, Copyright (c) Matt Kersley, 2014
	- https://github.com/mattkersley/Responsive-Menu
	- MIT license
	
8. Themeisle Activation Plugin
	- Copyright (c) 2017, Theme Isle
	- http://wordpress.org/themes/hestia/
	- GPL v2 License	
	
9. Scroll Reveal
	- Copyright (c) 2017 - @jlmakes
	- https://github.com/jlmakes/scrollreveal
	- MIT License	
	

All Other Resources, apart from the ones mentioned above have been created by me fall under GPL v3 license of the theme.	
	

## Screenshot & Image Credits

* https://www.pexels.com/photo/iphone-notebook-pen-working-34088/

License: CC0
Source: 

## Changelog


### Version 2.0.0

* Initial Release

### Version 2.0.1

* Support Added For Front Page Content
* Minor Bug Fixes

### Version 2.0.2

* Added Support For Full Width Page

### Version 2.0.3

* IH Custom Showcase Issue Fixed

### Version 2.0.4

* Google Fonts minor bug fixed.

### Version 2.0.5

* New Featured Area Added

### Version 2.0.6

* New Hero Section(About Us) Added

### Version 2.0.7

* Fixed Hero Widget
* Fixed Front Page
* Upgraded Customizer
* Bug Fixes
* Improved Code

### Version 2.0.8

* Added WooCommerce
* Fixed Front Page Options
* More Bug Fixes