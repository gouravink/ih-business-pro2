<?php
/**
 *
 *	PHP File for Custom WooCommerce Functions
 *
 */
 
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );

add_action( 'woocommerce_shop_loop_item_title', 'ihbp_template_loop_product_title', 10 );

function ihbp_template_loop_product_title() { 
	echo '<h2 class="woocommerce-loop-product__title title-font">' . get_the_title() . '</h2>'; 
} 