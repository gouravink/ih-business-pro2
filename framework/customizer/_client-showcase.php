<?php
function ihbp_customize_register_client_showcase($wp_customize) {
    $wp_customize->add_section(
        'ihbp_client_showcase_options',
        array(
            'title'     => __('Client Showcase','ih-business-pro'),
            'priority'  => 35,
            'panel' => 'ihbp_front_panel'
        )
    );

    $wp_customize->add_setting(
        'ihbp_client_showcase_enable',
        array( 'sanitize_callback' => 'ihbp_sanitize_checkbox' )
    );

    $wp_customize->add_control(
        'ihbp_client_showcase_enable', array(
            'settings' => 'ihbp_client_showcase_enable',
            'label'    => __( 'Enable', 'ih-business-pro' ),
            'section'  => 'ihbp_client_showcase_options',
            'type'     => 'checkbox',
            'default'  => false,
        )
    );

    $wp_customize->add_setting( 'ihbp_client_showcase_title' , array(
        'sanitize_callback' => 'sanitize_text_field'
    ) );

    $wp_customize->add_control(
        'ihbp_client_showcase_title', array(
        'label' => __('Title','ih-business-pro'),
        'section' => 'ihbp_client_showcase_options',
        'settings' => 'ihbp_client_showcase_title',
        'type' => 'text',
    ) );

    $wp_customize->add_setting(
        'ihbp_client_showcase_size',
        array(
            'sanitize_callback' => 'ihbp_sanitize_showcase_size',
            'default' => '150px'
        )
    );

    function ihbp_sanitize_showcase_size( $input ) {
        if ( in_array($input, array('115px','150px','200px','250px') ) )
            return $input;
        else
            return '150px';
    }

    $wp_customize->add_control(
        'ihbp_client_showcase_size',array(
            'label' => __('Select Showcase Size','ih-business-pro'),
            'settings' => 'ihbp_client_showcase_size',
            'section'  => 'ihbp_client_showcase_options',
            'type' => 'select',
            'choices' => array(
                '115px' => __('Small','ih-business-pro'),
                '150px' => __('Default','ih-business-pro'),
                '200px' => __('Large','ih-business-pro'),
                '250px' => __('Extra Large','ih-business-pro'),
            ),
        )
    );

    $wp_customize->add_setting(
        'ihbp_client_showcase_style',
        array(
            'sanitize_callback' => 'ihbp_sanitize_showcase_style',
            'default' => 'default'
        )
    );

    function ihbp_sanitize_showcase_style( $input ) {
        if ( in_array($input, array('default','carousel') ) )
            return $input;
        else
            return 'default';
    }

    $wp_customize->add_control(
        'ihbp_client_showcase_style',array(
            'label' => __('Select Showcase Layout','ih-business-pro'),
            'settings' => 'ihbp_client_showcase_style',
            'section'  => 'ihbp_client_showcase_options',
            'type' => 'select',
            'choices' => array(
                'default' => __('Default(Normal)','ih-business-pro'),
                'carousel' => __('Slider','ih-business-pro'),
            ),
        )
    );


    $wp_customize->add_setting('ihbp_client_logo',
        array(
            'sanitize_callback' => 'sanitize_text_field',
        )
    );

    $wp_customize->add_control(
        new ihbp_Display_Gallery_Control(
            $wp_customize, 'ihbp_client_logo',
            array (
                'setting' => 'ihbp_client_logo',
                'section' => 'ihbp_client_showcase_options',
                'label' => __('Client Logo ', 'ih-business-pro'),
            )
        )
    );
}
add_action('customize_register', 'ihbp_customize_register_client_showcase');