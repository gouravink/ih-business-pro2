<?php
function ihbp_customize_register_skins( $wp_customize ) {
	
	$wp_customize->get_section('colors')->panel = "ihbp_design_panel";
	$wp_customize->get_section('background_image')->panel = "ihbp_design_panel";
	$wp_customize->get_section('custom_css')->panel = "ihbp_design_panel";
	//Replace Header Text Color with, separate colors for Title and Description
	$wp_customize->get_control('header_textcolor')->label = __('Site Title Color','ih-business-pro');
	$wp_customize->add_setting('ihbp_header_desccolor', array(
	    'default'     => '#000',
	    'sanitize_callback' => 'sanitize_hex_color',
	));
	
	$wp_customize->add_control(new WP_Customize_Color_Control( 
		$wp_customize, 
		'ihbp_header_desccolor', array(
			'label' => __('Site Tagline Color','ih-business-pro'),
			'section' => 'colors',
			'settings' => 'ihbp_header_desccolor',
			'type' => 'color'
		) ) 
	);

    //Select the Default Theme Skin
    $wp_customize->add_section(
        'ihbp_skin_options',
        array(
            'title'     => __('Choose Skin','ih-business-pro'),
            'priority'  => 39,
            'panel'    => 'ihbp_design_panel'
        )
    );

    $wp_customize->add_setting(
        'ihbp_skin',
        array(
            'default'=> 'default',
            'sanitize_callback' => 'ihbp_sanitize_skin'
        )
    );

    $skins = array( 'default' => __('Default(White)','ih-business-pro'),
        'custom' => __('BUILD CUSTOM SKIN','ih-business-pro'),


    );

    $wp_customize->add_control(
        'ihbp_skin',array(
            'settings' => 'ihbp_skin',
            'section'  => 'ihbp_skin_options',
            'type' => 'select',
            'choices' => $skins,
        )
    );

    function ihbp_sanitize_skin( $input ) {
        if ( in_array($input, array('default','custom') ) )
            return $input;
        else
            return '';
    }
    //CUSTOM SKIN BUILDER

    $wp_customize->add_setting('ihbp_skin_var_background', array(
        'default'     => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'ihbp_skin_var_background', array(
            'label' => __('Primary Background','ih-business-pro'),
            'section' => 'ihbp_skin_options',
            'settings' => 'ihbp_skin_var_background',
            'active_callback' => 'ihbp_skin_custom',
            'type' => 'color'
        ) )
    );


    $wp_customize->add_setting('ihbp_skin_var_accent', array(
        'default'     => '#4cb2ed',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'ihbp_skin_var_accent', array(
            'label' => __('Primary Accent','ih-business-pro'),
            'section' => 'ihbp_skin_options',
            'settings' => 'ihbp_skin_var_accent',
            'type' => 'color',
            'active_callback' => 'ihbp_skin_custom',
        ) )
    );

    $wp_customize->add_setting('ihbp_skin_var_onaccent', array(
        'default'     => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'ihbp_skin_var_onaccent', array(
            'label' => __('Primary Onaccent','ih-business-pro'),
            'section' => 'ihbp_skin_options',
            'settings' => 'ihbp_skin_var_onaccent',
            'type' => 'color',
            'active_callback' => 'ihbp_skin_custom',
        ) )
    );

    $wp_customize->add_setting('ihbp_skin_var_content', array(
        'default'     => '#777',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'ihbp_skin_var_content', array(
            'label' => __('Content Color','ih-business-pro'),
            'description' => __('Must be Dark, like Black or Dark grey. Any darker color is acceptable.','ih-business-pro'),
            'section' => 'ihbp_skin_options',
            'settings' => 'ihbp_skin_var_content',
            'active_callback' => 'ihbp_skin_custom',
            'type' => 'color'
        ) )
    );

    function ihbp_skin_custom( $control ) {
        $option = $control->manager->get_setting('ihbp_skin');
        return $option->value() == 'custom' ;
    }
}
add_action( 'customize_register', 'ihbp_customize_register_skins' );