<?php
function ihbp_customize_register_fonts( $wp_customize ) {
		$wp_customize->add_section(
	    'ihbp_typo_options',
	    array(
	        'title'     => __('Google Web Fonts','ih-business-pro'),
	        'panel' => 'ihbp_design_panel',
	        'priority'  => 41,
	    )
	);
	
	$font_array = array('Ovo','Quattrocento Sans','Hind','Khula','Open Sans','Droid Sans','Droid Serif','Roboto','Roboto Condensed','Lato','Bree Serif','Oswald','Slabo','Lora','Source Sans Pro','Arimo','Bitter','Noto Sans');
	$fonts = array_combine($font_array, $font_array);
	
	$wp_customize->add_setting(
		'ihbp_title_font',
		array(
			'default'=> 'Ovo',
			'sanitize_callback' => 'ihbp_sanitize_gfont' 
			)
	);
	
	function ihbp_sanitize_gfont( $input ) {
		if ( in_array($input, array('Ovo','Quattrocento Sans','Hind','Khula','Open Sans','Droid Sans','Droid Serif','Roboto','Roboto Condensed','Lato','Bree Serif','Oswald','Slabo','Lora','Source Sans Pro','Arimo','Bitter','Noto Sans') ) )
			return $input;
		else
			return 'ih-business-pro';	
	}
	
	$wp_customize->add_control(
		'ihbp_title_font',array(
				'label' => __('Title','ih-business-pro'),
				'settings' => 'ihbp_title_font',
				'section'  => 'ihbp_typo_options',
				'type' => 'select',
				'choices' => $fonts,
			)
	);
	
	$wp_customize->add_setting(
		'ihbp_body_font',
			array(	'default'=> 'Quattrocento Sans',
					'sanitize_callback' => 'ihbp_sanitize_gfont' )
	);
	
	$wp_customize->add_control(
		'ihbp_body_font',array(
				'label' => __('Body','ih-business-pro'),
				'settings' => 'ihbp_body_font',
				'section'  => 'ihbp_typo_options',
				'type' => 'select',
				'choices' => $fonts
			)
	);


    //Page and Post content Font size start
    $wp_customize->add_setting(
        'ihbp_content_page_post_fontsize_set',
        array(
            'default' => 'default',
            'sanitize_callback' => 'ihbp_sanitize_content_size'
        )
    );
    function ihbp_sanitize_content_size( $input ) {
        if ( in_array($input, array('default','small','medium','large','extra-large') ) )
            return $input;
        else
            return '';
    }

    $wp_customize->add_control(
        'ihbp_content_page_post_fontsize_set', array(
            'settings' => 'ihbp_content_page_post_fontsize_set',
            'label'    => __( 'Page/Post Font Size','ih-business-pro' ),
            'description' => __('Choose your font size. This is only for Posts and Pages. It wont affect your blog page.','ih-business-pro'),
            'section'  => 'ihbp_typo_options',
            'type'     => 'select',
            'choices' => array(
                'default'   => 'Default',
                'small' => 'Small',
                'medium'   => 'Medium',
                'large'  => 'Large',
                'extra-large' => 'Extra Large',
            ),
        )
    );

    //Page and Post content Font size end


    //site title Font size start
    $wp_customize->add_setting(
        'ihbp_content_site_title_fontsize_set',
        array(
            'default' => '42',
            'sanitize_callback' => 'absint',
        )
    );

    $wp_customize->add_control(
        'ihbp_content_site_title_fontsize_set', array(
            'settings' => 'ihbp_content_site_title_fontsize_set',
            'label'    => __( 'Site Title Font Size','ih-business-pro' ),
            'description' => __('Choose your font size. This is only for Site Title.','ih-business-pro'),
            'section'  => 'ihbp_typo_options',
            'type'     => 'number',
        )
    );
    //site title Font size end

    //site description Font size start
    $wp_customize->add_setting(
        'ihbp_content_site_desc_fontsize_set',
        array(
            'default' => '15',
            'sanitize_callback' => 'absint',
        )
    );

    $wp_customize->add_control(
        'ihbp_content_site_desc_fontsize_set', array(
            'settings' => 'ihbp_content_site_desc_fontsize_set',
            'label'    => __( 'Site Description Font Size','ih-business-pro' ),
            'description' => __('Choose your font size. This is only for Site Description.','ih-business-pro'),
            'section'  => 'ihbp_typo_options',
            'type'     => 'number',
        )
    );
    //site description Font size end

}
add_action( 'customize_register', 'ihbp_customize_register_fonts' );