<?php
/*
** Customizer Controls 
*/
if (class_exists('WP_Customize_Control')) {
	class ihbp_WP_Customize_Category_Control extends WP_Customize_Control {
        /**
         * Render the control's content.
         */
        public function render_content() {
            $dropdown = wp_dropdown_categories(
                array(
                    'name'              => '_customize-dropdown-categories-' . $this->id,
                    'echo'              => 0,
                    'show_option_none'  => __( '&mdash; Select &mdash;', 'ih-business-pro' ),
                    'option_none_value' => '0',
                    'selected'          => $this->value(),
                )
            );
 
            $dropdown = str_replace( '<select', '<select ' . $this->get_link(), $dropdown );
 
            printf(
                '<label class="customize-control-select"><span class="customize-control-title">%s</span> %s</label>',
                $this->label,
                $dropdown
            );
        }
    }
}  

if (class_exists('WP_Customize_Control')) {
	class ihbp_WP_Customize_Upgrade_Control extends WP_Customize_Control {
        /**
         * Render the control's content.
         */
        public function render_content() {
             printf(
                '<label class="customize-control-upgrade"><span class="customize-control-title">%s</span> %s</label>',
                $this->label,
                $this->description
            );
        }
    }
}

if (class_exists('WP_Customize_Control')) {
    class ihbp_Display_Gallery_Control extends WP_Customize_Control
    {
        public $type = 'gallery';

        public function render_content()
        {
            ?>
            <label>
                <span class="customize-control-title">
                    <?php echo esc_html($this->label); ?>
                </span>

                <?php if ($this->description) { ?>
                    <span class="description customize-control-description">
                        <?php echo wp_kses_post($this->description); ?>
                    </span>
                <?php } ?>

                <div class="gallery-screenshot clearfix">
                    <?php
                    {
                        $ids = explode(',', $this->value());
                        foreach ($ids as $attachment_id) {
                            $img = wp_get_attachment_image_src($attachment_id, 'thumbnail');
                            echo '<div class="screen-thumb"><img src="' . esc_url($img[0]) . '" /></div>';
                        }
                    }
                    ?>
                </div>

                <input id="edit-gallery" class="button upload_gallery_button" type="button"
                       value="<?php esc_html_e('Add/Edit Gallery', 'ih-business-pro') ?>"/>
                <input id="clear-gallery" class="button upload_gallery_button" type="button"
                       value="<?php esc_html_e('Clear', 'ih-business-pro') ?>"/>
                <input type="hidden" class="gallery_values" <?php echo esc_attr($this->link()) ?>
                       value="<?php echo esc_attr($this->value()); ?>">
            </label>
            <?php
        }
    }
}