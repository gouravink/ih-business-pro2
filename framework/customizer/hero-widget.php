<?php
function ihbp_customize_register_hero_widget($wp_customize) {
    //HERO 1
    $hero_widget_section = (object)$wp_customize->get_section('sidebar-widgets-cta-hero');
    $hero_widget_section ->panel = 'ihbp_hero_panel';

    $wp_customize->add_panel('ihbp_hero_panel',
        array(
            'title' => __('Hero Sections', 'ih-business-pro'),
            'priority' => 30,
        )
    );

    $wp_customize->add_section('ihbp_hero1_section',
        array(
            'title' => __('Hero Widget', 'ih-business-pro'),
            'priority' => 0,
            'panel' => 'ihbp_hero_panel',
        )
    );

    $wp_customize->add_setting('ihbp_hero_enable',
        array(
	        'default'			=> false,
            'sanitize_callback' => 'ihbp_sanitize_checkbox'
        ));
    $wp_customize->add_control('ihbp_hero_enable',
        array(
            'setting' => 'ihbp_hero_enable',
            'section' => 'ihbp_hero1_section',
            'label' => __('Enable Hero Widget', 'ih-business-pro'),
            'type' => 'checkbox',
        )
    );

    $wp_customize->add_setting('ihbp_hero1_selectpages',
        array(
            'sanitize_callback' => 'absint'
        )
    );

    $wp_customize->add_control('ihbp_hero1_selectpages',
        array(
            'setting' => 'ihbp_hero1_selectpages',
            'section' => 'ihbp_hero1_section',
            'label' => __('Title', 'ih-business-pro'),
            'description' => __('Select a Product to display Title', 'ih-business-pro'),
            'type' => 'dropdown-pages',
        )
    );


    $wp_customize->add_setting('ihbp_hero1_full_content',
        array(
            'sanitize_callback' => 'ihbp_sanitize_checkbox'
        )
    );

    $wp_customize->add_control('ihbp_hero1_full_content',
        array(
            'setting' => 'ihbp_hero1_full_content',
            'section' => 'ihbp_hero1_section',
            'label' => __('Show Full Content insted of excerpt', 'ih-business-pro'),
            'type' => 'checkbox',
        )
    );

    $wp_customize->add_setting('ihbp_hero1_display_only_widget',
        array(
            'sanitize_callback' => 'ihbp_sanitize_checkbox'
        )
    );

    $wp_customize->add_control('ihbp_hero1_display_only_widget',
        array(
            'setting' => 'ihbp_hero1_display_only_widget',
            'section' => 'ihbp_hero1_section',
            'label' => __('Display Only Widget', 'ih-business-pro'),
            'type' => 'checkbox',
        )
    );

    $wp_customize->add_setting('ihbp_hero1_button',
        array(
            'sanitize_callback' => 'sanitize_text_field'
        )
    );

    $wp_customize->add_control('ihbp_hero1_button',
        array(
            'setting' => 'ihbp_hero1_button',
            'section' => 'ihbp_hero1_section',
            'label' => __('Button Name', 'ih-business-pro'),
            'description' => __('Leave blank to disable Button.', 'ih-business-pro'),
            'type' => 'text',
        )
    );

    $wp_customize->add_setting('ihbp_hero_background_image',
        array(
            'sanitize_callback' => 'esc_url_raw',
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize, 'ihbp_hero_background_image',
            array (
                'setting' => 'ihbp_hero_background_image',
                'section' => 'ihbp_hero1_section',
                'label' => __('Background Image', 'ih-business-pro'),
                'description' => __('Upload an image to display in background of the area', 'ih-business-pro'),
            )
        )
    );
}
add_action('customize_register', 'ihbp_customize_register_hero_widget');