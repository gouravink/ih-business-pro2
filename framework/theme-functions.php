<?php
/*
 * @package , Copyright Rohit Tripathi, inkhive.com
 * This file contains Custom Theme Related Functions.
 */

//Import Admin Modules
require get_template_directory() . '/framework/admin_modules/register_styles.php';
require get_template_directory() . '/framework/admin_modules/register_widgets.php';
require get_template_directory() . '/framework/admin_modules/customizer_scripts.php';
require get_template_directory() . '/framework/admin_modules/theme_setup.php';
require get_template_directory() . '/framework/admin_modules/nav_walkers.php';
require get_template_directory() . '/framework/admin_modules/exclude_posts.php';
require get_template_directory() . '/framework/admin_modules/admin_styles.php';
require get_template_directory() . '/framework/admin_modules/section_titles.php';


/*
** Function to check if Sidebar is enabled on Current Page 
*/
function ihbp_load_sidebar() {
	$load_sidebar = true;
	if ( get_theme_mod('ihbp_disable_sidebar') ) :
		$load_sidebar = false;
	elseif( get_theme_mod('ihbp_disable_sidebar_home') && is_home() )	:
		$load_sidebar = false;
	elseif( get_theme_mod('ihbp_disable_sidebar_front') != '' && is_front_page() ) :
		$load_sidebar = false;
	endif;
	
	return  $load_sidebar;
}

/*
** Add Body Class
*/
function ihbp_body_class( $classes ) {
	
	$sidebar_class_name =  ihbp_load_sidebar() ? "sidebar-enabled" : "sidebar-disabled" ;
    return array_merge( $classes, array( $sidebar_class_name ) );   
}
add_filter( 'body_class', 'ihbp_body_class' );


/*
**	Determining Sidebar and Primary Width
*/
function ihbp_primary_class() {
	$sw = esc_html(get_theme_mod('ihbp_sidebar_width',4));
	$class = "col-md-".(12-$sw);
	
	if ( !ihbp_load_sidebar() ) 
		$class = "col-md-12";
	
	echo $class;
}
add_action('ihbp_primary-width', 'ihbp_primary_class');

function ihbp_secondary_class() {
	$sw = esc_html(get_theme_mod('ihbp_sidebar_width',4));
	$class = "col-md-".$sw;
	
	echo $class;
}
add_action('ihbp_secondary-width', 'ihbp_secondary_class');

/*
**	Helper Function to Convert Colors
*/
function ihbp_hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);
   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   return implode(",", $rgb); // returns the rgb values separated by commas
   //return $rgb; // returns an array with the rgb values
}

function ihbp_fade($color, $val) {
	return "rgba(".ihbp_hex2rgb($color).",". $val.")";
}

//Function to Trim Excerpt Length & more..
function ihbp_excerpt_length( $length ) {
	return 23;
}
add_filter( 'excerpt_length', 'ihbp_excerpt_length', 999 );

function ihbp_excerpt_more( $more ) {
	return '&hellip;';
}
add_filter( 'excerpt_more', 'ihbp_excerpt_more' );



/*
** Function to Get Theme Layout 
*/
function ihbp_get_blog_layout(){
	$ldir = 'framework/layouts/content';
	if (get_theme_mod('ihbp_blog_layout') ) :
		get_template_part( $ldir , get_theme_mod('ihbp_blog_layout') );
	else :
		get_template_part( $ldir ,'ihbp');	
	endif;	
}
add_action('ihbp_blog_layout', 'ihbp_get_blog_layout');


/**
 * Include Meta Boxes.
 */

require get_template_directory() . '/framework/metaboxes/page-attributes.php';
require get_template_directory() . '/framework/metaboxes/display-options.php';



/*
** Function to Set Masonry Class 
*/
function ihbp_set_masonry_class(){
	if ( get_theme_mod('ihbp_blog_layout') != "masonry" ) :
		//DO NOTHING
	else :
		echo "masonry-main";	
	endif;	
}
add_action('ihbp_masonry_class', 'ihbp_set_masonry_class');



/*
** Load Custom Widgets
*/

require get_template_directory() . '/framework/widgets/recent-posts.php';


/*
 * Pagination Function. Implements core paginate_links function.
 */
function ihbp_pagination() {
	global $wp_query;
	$big = 12345678;
	$page_format = paginate_links( array(
	    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	    'format' => '?paged=%#%',
	    'current' => max( 1, get_query_var('paged') ),
	    'total' => $wp_query->max_num_pages,
	    'type'  => 'array'
	) );
	if( is_array($page_format) ) {
	            $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
	            echo '<div class="pagination"><div><ul>';
	            echo '<li><span>'. $paged . ' of ' . $wp_query->max_num_pages .'</span></li>';
	            foreach ( $page_format as $page ) {
	                    echo "<li>$page</li>";
	            }
	           echo '</ul></div></div>';
	 }
}

//Quick Fixes for Custom Post Types.
function ihbp_pagination_queried( $query ) {
	$big = 12345678;
	$page_format = paginate_links( array(
	    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	    'format' => '?paged=%#%',
	    'current' => max( 1, get_query_var('paged') ),
	    'total' => $query->max_num_pages,
	    'type'  => 'array'
	) );
	if( is_array($page_format) ) {
	            $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
	            echo '<div class="pagination"><div><ul>';
	            echo '<li><span>'. $paged . __(' of ', 'ihbp') . $query->max_num_pages .'</span></li>';
	            foreach ( $page_format as $page ) {
	                    echo "<li>$page</li>";
	            }
	           echo '</ul></div></div>';
	 }
}


// Blog Section for Static Front Page


function ihbp_static_page_blog() { ?>
	
	<div class="section-title">
		<span><?php _e('From the Blog', 'ih-business-pro'); ?></span>
	</div>
	
	<?php
	$qa = array (
	            'post_type'              => 'post',
	            'ignore_sticky_posts'    => true,
				'posts_per_page'		 => 3,
	        );
	        
	        $front_blog	=	new WP_Query( $qa ); ?>
			
			<div class="blog_posts">
				<?php
		            /* Start the Loop */
		            while ( $front_blog->have_posts() ) : $front_blog->the_post();
		
		                /* Include the Post-Format-specific template for the content.
		                 */
		                get_template_part('framework/layouts/content', 'ihbp');
		
		
		            endwhile;
		        ?>
			</div>
<?php
}
add_action('blog_before_content', 'ihbp_static_page_blog');