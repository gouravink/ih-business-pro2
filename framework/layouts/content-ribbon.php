<?php
/**
 * @package Hanne
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-4 col-sm-4 ribbon'); ?>>
    <div class="zig-zag"></div>
    <div class="entry-header">
        <h1 class="entry-title title-font">
            <a class="" href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
        </h1>
        </div>


    <div class="featured-thumb col-md-12 col-sm-12">
        <?php if (has_post_thumbnail()) : ?>
            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>"><?php the_post_thumbnail('ihbp-pop-thumb'); ?></a>
        <?php else: ?>
            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>"><img src="<?php echo get_template_directory_uri()."/assets/images/placeholder2.jpg"; ?>"></a>
        <?php endif; ?>
    </div><!--.featured-thumb-->


</article><!-- #post-## -->
