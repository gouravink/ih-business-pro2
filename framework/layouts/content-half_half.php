<?php
/**
 * @package Hanne
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('col-md-3 col-sm-6 half_half'); ?>>
    <div class="article-border">
            <?php if (has_post_thumbnail()) : ?>
                <a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>"><?php the_post_thumbnail('ihbp-thumb'); ?></a>
            <?php else: ?>
                <a href="<?php the_permalink() ?>" title="<?php the_title_attribute() ?>"><img src="<?php echo get_template_directory_uri()."/assets/images/placeholder3.jpg"; ?>"></a>
            <?php endif; ?>
    <div class="hh-title">
        <h1 class="entry-title title-font"><a class="" href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
    </div>
    </div>
</article><!-- #post-## -->