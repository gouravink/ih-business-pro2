<div id="client-showcase" class="featured-section-area">
    <div class="section-title">
        <span><?php echo esc_html(get_theme_mod('ihbp_client_showcase_title')); ?></span>
    </div>

    <?php get_template_part('modules/footer/client-showcase', get_theme_mod('ihbp_client_showcase_style', 'default')); ?>
</div>