<div id="hero-widget" class="hero-content">
    <?php if ( get_theme_mod('ihbp_hero_background_image') !='' ): ?>
        <div class="layer"></div>
    <?php endif; ?>
    <div class="container hero-container">
    <?php
	    
	$hero_post	=	get_post( get_theme_mod('ihbp_hero1_selectpages') );

        $class = is_active_sidebar('cta-hero') ?  'col-md-8 col-sm-8' : 'col-md-12 col-sm-12 centered' ; ?>
        
        <?php if ( !get_theme_mod('ihbp_hero1_display_only_widget') ) : ?>
        
	        <div class="<?php echo $class; ?> h-content">
	            <h1 class="title">
	                <?php echo $hero_post->post_title; ?>
	            </h1>

	            <?php if( get_theme_mod( 'ihbp_hero1_full_content', true ) ) : ?>
	            
	                <div class="excerpt">
	                    <?php echo $hero_post->post_content; ?>
	                </div>

	            <?php else : ?>
	                <div class="excerpt">
	                    <?php echo substr($hero_post->post_content, 0, 300) . "..."; ?>
	                </div>
	            <?php endif; ?>

	            <?php if(get_theme_mod('ihbp_hero1_button') != ''): ?>
	                <a href="<?php the_permalink(); ?>" class="more-button">
	                    <?php echo esc_html(get_theme_mod('ihbp_hero1_button')); ?>
	                </a>
	            <?php endif;	?>
	        </div>
	        
	    <?php endif; ?>
	        
        
        <?php if ( is_active_sidebar('cta-hero') ) :
            $classw = get_theme_mod('ihbp_hero1_display_only_widget') ?  'col-md-12 col-sm-12 centered' : 'col-md-4 col-sm-4' ; ?>
        <div class="<?php echo $classw; ?> f-widget">
            <?php dynamic_sidebar( 'cta-hero' ); ?>
        </div>
        <?php endif; ?>
    </div>
</div>

