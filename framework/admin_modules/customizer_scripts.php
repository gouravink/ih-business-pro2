<?php
function ihbp_custom_wp_customizer_styles() {
    wp_enqueue_script( 'gallery', get_template_directory_uri() . '/assets/js/custom-gallery.js', array('jquery'), false, true );
}
add_action('customize_controls_enqueue_scripts', 'ihbp_custom_wp_customizer_styles');