<?php
/**
 * Enqueue Scripts for Admin
 */
function ihbp_custom_wp_admin_style() {
    wp_enqueue_media();
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/font-awesome/css/font-awesome.min.css' );
    wp_enqueue_style( 'ih-business-pro-admin_css', get_template_directory_uri() . '/assets/css/admin.css' );
    wp_enqueue_script( 'ih-business-pro-admin_scripts', get_template_directory_uri() . '/assets/js/admin-scripts.js' );

}
add_action( 'admin_enqueue_scripts', 'ihbp_custom_wp_admin_style' );