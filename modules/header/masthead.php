<?php
/**
 *
 *	Masthead of the Theme
 *
**/
$header_style = get_theme_mod('ihbp_header_styles_set','default');

switch ( $header_style ) {

	case 'style1' :
		get_template_part('modules/header/layouts/header','style1');
		break;
		
	case 'style2' :
		get_template_part('modules/header/layouts/header','style2');
		break;
		
	case 'style3' :
		get_template_part('modules/header/layouts/header','style3');
		break;
	
	default :
		get_template_part('modules/header/layouts/header','default');

}
