<div id="top-bar">
	<div class="container top-bar-inner">
		<div id="contact-icons">
			
			<div class="icon mail">
				<?php if (get_theme_mod('ihbp_mail_id')) : ?>
					<span class="fa fa-envelope"></span>
					<span class="value"><?php echo esc_html( get_theme_mod('ihbp_mail_id') ); ?></span>
				<?php endif; ?>
			</div>
			
			<div class="icon phone">
				<?php if (get_theme_mod('ihbp_phone')) : ?>
					<span class="fa fa-phone"></span>
					<span class="value"><?php echo esc_html( get_theme_mod('ihbp_phone') ); ?></span>
				<?php endif; ?>
			</div>
			
		</div>
		
		<div id="social-icons">
			<?php get_template_part('modules/social/social', 'fa'); ?>
		</div>
	</div>
</div>