<?php $image_ids = get_theme_mod('ihbp_client_logo');
$image_ids = explode(',', $image_ids );
?>
<div class="client-logo">
    <?php
    foreach($image_ids as $id) :
        $image_urls = wp_get_attachment_url($id);
        ?>
        <img src="<?php echo $image_urls; ?>" />
    <?php endforeach; ?>
</div>