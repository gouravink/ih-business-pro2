<div class="cs-carousel-container">
    <div class="swiper-wrapper">

    <?php $image_ids = get_theme_mod('ihbp_client_logo');
    $image_ids = explode(',', $image_ids );
    ?>
    <?php
    foreach($image_ids as $id) :
    $image_urls = wp_get_attachment_url($id);
    ?>

        <div class="swiper-slide">
                <img src="<?php echo $image_urls; ?>" />
        </div>
    <?php endforeach; ?>
    </div>

    <div class="swiper-button-next sbncp_c swiper-button-black"></div>
    <div class="swiper-button-prev sbpcp_c swiper-button-black"></div>

</div>