/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {
	// Site title and description
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );
	
	// Social Icons
	var	count 	=	[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
	$.each( count, function( index ) {
		wp.customize( 'ihbp_social_' + index, function( value ) {
	        value.bind( function( to ) {
	            var ClassNew	=	'fa fa-fw fa-' + to;
	            jQuery('#social-icons' ).find( 'i:eq(' + (index-1) + ')' ).attr( 'class', ClassNew );
	        });
	    });
	});
	
	// E-Mail
	wp.customize( 'ihbp_mail_id', function( value ) {
		value.bind( function( to ) {
			$( '#contact-icons .mail' ).html( '<span class="fa fa-envelope"></span><span class="value">' + to + '</span>' );
		} );
	} );
	
	// Telephone Number
	wp.customize( 'ihbp_phone', function( value ) {
		value.bind( function( to ) {
			$( '#contact-icons .phone' ).html( '<span class="fa fa-phone"></span><span class="value">' + to + '</span>' );
		} );
	} );
	
	// Hero Text
	wp.customize( 'ihbp_heading', function( value ) {
		value.bind( function( to ) {
			$( '.hero-content .container' ).html( '<h2 class="hero-title">' + to + '</h2>' );
		} );
	} );
	
	 //Sidebar
    wp.customize( 'ihbp_sidebar_width', function( value ) {
        value.bind( function( to ) {
            var SidebarWidth    =   (to * 100) / 12;
            $('#secondary').css('width', SidebarWidth + '%' );
            $('#primary, #primary-mono').css('width', 100 - SidebarWidth + '%' );
        } );
    } );

} )( jQuery );


