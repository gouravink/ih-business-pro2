/**
 *
 *	JS code for Customizer Controls
 *
**/

( function() {
	
	wp.customize.bind( 'ready', function() {
		
		var heroWidget	=	['ihbp_hero1_selectpages','ihbp_hero1_full_content','ihbp_hero1_display_only_widget','ihbp_hero1_button','ihbp_hero_background_image'];
		
		wp.customize( 'ihbp_hero_enable', function( setting ) {
				var visibility	=	 function() {
				if ( '' == setting.get() ) {
					$.each( heroWidget, function( index, controlIDs ) {
						wp.customize.control( controlIDs ).container.hide();
					});
				} else {
					$.each( heroWidget, function( index, controlIDs ) {
						wp.customize.control( controlIDs ).container.show();	
					});
				}
			}
			
			visibility();
			setting.bind( visibility );
		});
		
		var clientShowcase	=	['ihbp_client_showcase_title','ihbp_client_showcase_size','ihbp_client_showcase_style','ihbp_client_logo'];
		
		wp.customize( 'ihbp_client_showcase_enable', function( setting ) {
				var visibility	=	 function() {
				if ( '' == setting.get() ) {
					$.each( clientShowcase, function( index, controlIDs ) {
						wp.customize.control( controlIDs ).container.hide();
					});
				} else {
					$.each( clientShowcase, function( index, controlIDs ) {
						wp.customize.control( controlIDs ).container.show();	
					});
				}
			}
			
			visibility();
			setting.bind( visibility );
		});
		
		var sidebarOptions	=	['ihbp_disable_sidebar_home','ihbp_disable_sidebar_front','ihbp_sidebar_width'];
		
		wp.customize( 'ihbp_disable_sidebar', function( setting ) {
				var visibility	=	 function() {
				if ( '1' == setting.get() ) {
					$.each( sidebarOptions, function( index, controlIDs ) {
						wp.customize.control( controlIDs ).container.hide();
					});
				} else {
					$.each( sidebarOptions, function( index, controlIDs ) {
						wp.customize.control( controlIDs ).container.show();	
					});
				}
			}
			
			visibility();
			setting.bind( visibility );
		});
		
	});
})( jQuery );