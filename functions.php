<?php
/**
 *  functions and definitions
 *
 * @package 
 *
 * Include the Custom Functions of the Theme.
 */
require get_template_directory() . '/framework/theme-functions.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Implement the Custom CSS Mods.
 */
require get_template_directory() . '/inc/css-mods.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Costom Skins.
 */
require get_template_directory() . '/framework/designer/designer.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/framework/customizer/init.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


/**
 *	Load PHP File for WooCommerce
 */
 
 if ( class_exists('woocommerce') ) :
 	require get_template_directory() . '/framework/woocommerce.php';
 endif;